import { createStore as reduxCreateStore } from 'redux';

const reducer = (state, action) => {
  if (action.type === `UPDATE_NAV_CATEGORY`) {
    return Object.assign({}, state, {
      selectedCategory: action.selectedCategory,
    });
  }

  if (action.type === `UPDATE_NAV_SUBCATEGORY`) {
    return Object.assign({}, state, {
      selectedSubCategory: action.selectedSubCategory,
    });
  }

  if (action.type === `UPDATE_NAV_PRODUCTTYPE`) {
    return Object.assign({}, state, {
      selectedProductType: action.productType,
    });
  }

  if (action.type === 'UPDATE_NAV_FROM_SLUG') {
    return Object.assign({}, state, {
      selectedCategory: action.selectedCategory,
      selectedSubCategory: action.selectedSubCategory,
      selectedProductType: action.productType,
    });
  }

  if (action.type === 'RESET_STATE') {
    return Object.assign({}, state, {
      selectedCategory: '',
      selectedSubCategory: '',
      selectedProductType: '',
    });
  }

  return state;
};

const initialState = {
  selectedCategory: '',
  selectedSubCategory: '',
  selectedProductType: '',
};

const createStore = () => reduxCreateStore(reducer, initialState);

export default createStore;
