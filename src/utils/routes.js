export const getCategoryRoute = category => `/${category}`;

export const getProductTypeRoute = (parentCategory, productType) => `/${parentCategory}/${productType}`;

export const getSubCategoryItemRoute = (parentCategory, subCategory, productType) => `/${parentCategory}/${subCategory}/${productType}`;
