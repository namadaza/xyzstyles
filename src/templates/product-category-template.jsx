import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, navigate } from 'gatsby';
import { connect } from 'react-redux';
import Img from 'gatsby-image';

import Layout from '../components/layout';
import SEO from '../components/seo';
import './styles/product-category-template.scss';

class ProductCategoryTemplateUI extends Component {
  static propTypes = {
    data: PropTypes.shape({}).isRequired,
    pageContext: PropTypes.shape({}).isRequired,
    updateNavFromSlug: PropTypes.func.isRequired,
  };

  state = { noData: false };

  componentDidMount() {
    const { data } = this.props;
    if (!data || !data.productsJson || !data.allFile) {
      navigate('/404');
      this.setState(() => ({ noData: true }));
    }
  }

  render() {
    const { data, pageContext, updateNavFromSlug } = this.props;
    const { noData } = this.state;
    const { pageTitle, category, subCategory, productType } = pageContext;

    if (noData) return null;
    updateNavFromSlug(category, subCategory, productType);

    const productDetails = subCategory
      ? data.productsJson.subCategories.filter(item => item.name === subCategory)[0].products.filter(item => item.productType === productType)[0]
          .productDetails
      : data.productsJson.subCategories[0].products.filter(item => item.productType === productType)[0].productDetails;

    const productImageObjects = productDetails.reduce((productImages, productItem) => {
      const productImageObject = data.allFile.edges.filter(
        edge =>
          edge.node.childImageSharp.fluid.originalName
            .split('.')
            .slice(0, -1)
            .join('.') === productItem.id
      );
      /* eslint-disable no-param-reassign */
      productImages[productItem.id] = productImageObject.length !== 0 ? productImageObject[0].node.childImageSharp : null;
      return productImages;
    }, {});

    return (
      <Layout>
        <SEO title={pageTitle} />
        <div className="products-page">
          <h1 className="text-center">{pageTitle}*</h1>
          <div className="disclaimer text-center pb-4">*prices indicate our average FOB cost</div>
          <div className="product-items-container d-flex flex-wrap justify-content-between">
            {productDetails.map(productItem => (
              <div className="product-item mb-5" key={productItem.id}>
                {productImageObjects[productItem.id] && <Img fixed={productImageObjects[productItem.id].fixed} />}
                <div className="product-details text-right">
                  {`${productItem.name} `}
                  <span>~{productItem.price}</span>
                  {`/piece`}
                </div>
              </div>
            ))}
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  updateNavFromSlug: (newCategory, newSubCategory, newProductType) =>
    dispatch({
      type: `UPDATE_NAV_FROM_SLUG`,
      selectedCategory: newCategory,
      selectedSubCategory: newSubCategory,
      selectedProductType: newProductType,
    }),
});

const ProductCategoryTemplate = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductCategoryTemplateUI);

export default ProductCategoryTemplate;

export const productImageFragment = graphql`
  fragment productImageFormat on File {
    childImageSharp {
      fluid(maxWidth: 600, maxHeight: 900) {
        originalName
        ...GatsbyImageSharpFluid
      }
      fixed(width: 500, height: 800) {
        originalName
        ...GatsbyImageSharpFixed
      }
    }
  }
`;

// Prefer to use pageQuery for templates to avoid
// difficult to debug issues around context types
export const pageQuery = graphql`
  query($category: String!, $productImages: String!) {
    productsJson(category: { eq: $category }) {
      category
      subCategories {
        name
        products {
          productType
          productDetails {
            id
            name
            price
          }
        }
      }
    }
    allFile(filter: { relativeDirectory: { eq: $productImages } }) {
      edges {
        node {
          relativeDirectory
          ...productImageFormat
        }
      }
    }
  }
`;
