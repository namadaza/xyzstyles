import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Slider from 'react-slick';

import Layout from '../components/layout';
import SEO from '../components/seo';

import './styles/index.scss';

const IndexPageUI = ({ resetState }) => {
  resetState();
  const sliderSettings = {
    dots: false,
    infinite: true,
    autoplaySpeed: 2000,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    accessibility: false,
  };

  return (
    <>
      <SEO title="Home" keywords={[`xyzstyles`, `application`, `react`]} />
      <div className="index">
        <Slider {...sliderSettings}>
          <div>
            <div className="item">1</div>
          </div>
          <div>
            <div className="item">2</div>
          </div>
        </Slider>
      </div>
      <Layout>
        <div />
      </Layout>
    </>
  );
};

IndexPageUI.propTypes = {
  resetState: PropTypes.func.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  resetState: () => dispatch({ type: `RESET_STATE` }),
});

const IndexPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexPageUI);

export default IndexPage;
