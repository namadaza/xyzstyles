import React from 'react';
import PropTypes from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';
import { Container, Row, Col } from 'reactstrap';

import Header from './header';
import Navbar from './navbar';

import './styles/bootstrap.min.css';
import './styles/layout.scss';

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <Container fluid className="layout d-flex flex-column">
        <Row className="sticky-top bg-white mx-5">
          <Col>
            <Header siteTitle={data.site.siteMetadata.title} />
          </Col>
        </Row>
        <Row className="mx-5">
          <Col lg={2} md={3}>
            <Navbar />
          </Col>
          <Col lg={10} md={9}>
            {children}
          </Col>
        </Row>
        <Row className="flex-fill" />
        <Row>
          <Col className="footer text-center">
            <footer>XYZ Styles© 2019</footer>
          </Col>
        </Row>
      </Container>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
