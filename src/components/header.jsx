import { Link } from 'gatsby';
import React from 'react';
import { Row, Col } from 'reactstrap';

import Logo from './logo';
import './styles/header.scss';

const Header = () => (
  <Row className="header xyz-header">
    <Col lg={2} className="logo mt-3">
      <Link to="/">
        <Logo />
      </Link>
    </Col>
  </Row>
);

export default Header;
