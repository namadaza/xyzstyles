import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'gatsby';
import { connect } from 'react-redux';

import { getProductTypeRoute, getSubCategoryItemRoute } from '../utils/routes';

class NavbarSubCategoryUI extends Component {
  static propTypes = {
    category: PropTypes.string,
    subCategories: PropTypes.arrayOf(PropTypes.shape({})),
    updateNavSubCategory: PropTypes.func.isRequired,
    selectedSubCategory: PropTypes.string,
    selectedProductType: PropTypes.string,
  };

  static defaultProps = {
    category: '',
    subCategories: [{}],
    selectedSubCategory: '',
    selectedProductType: '',
  };

  updateSubCategory = event => {
    const { updateNavSubCategory } = this.props;
    const { currentTarget } = event;
    const newSubCategory = currentTarget.getElementsByClassName('d-none')[0].textContent;
    updateNavSubCategory(newSubCategory);
  };

  render() {
    const { subCategories, category, selectedSubCategory, selectedProductType } = this.props;

    return subCategories.map(subCategory => (
      <div key={subCategory.name} className={classNames('navbar-subcategory', { 'pl-2': subCategory.name !== 'default' })}>
        {subCategory.name !== 'default' && (
          <button type="button" className="navbar-button navbar-subgroup-button m-0 p-0" onClick={this.updateSubCategory}>
            <div className="d-none">{subCategory.name}</div>
            <div className={classNames('subgroup-category text-uppercase', { 'font-weight-bold': selectedSubCategory === subCategory.name })}>
              {subCategory.displayName}
            </div>
          </button>
        )}
        {(selectedSubCategory === subCategory.name || subCategory.name === 'default') && (
          <div className="link-container text-capitalize pb-3 mb-3">
            {subCategory.products.map(product => (
              <Link
                to={
                  subCategory.name !== 'default'
                    ? getSubCategoryItemRoute(category, subCategory.name, product.productType)
                    : getProductTypeRoute(category, product.productType)
                }
                key={product.productType}
              >
                <div
                  className={classNames('navbar-subcategory-button d-block font-weight-normal', {
                    'font-weight-bold': selectedProductType === product.productType,
                  })}
                >
                  {product.productType}
                </div>
              </Link>
            ))}
          </div>
        )}
      </div>
    ));
  }
}

const mapStateToProps = ({ selectedSubCategory, selectedProductType }) => ({ selectedSubCategory, selectedProductType });

const mapDispatchToProps = dispatch => ({
  updateNavSubCategory: newCategory => dispatch({ type: `UPDATE_NAV_SUBCATEGORY`, selectedSubCategory: newCategory }),
});

const NavbarSubCategory = connect(
  mapStateToProps,
  mapDispatchToProps
)(NavbarSubCategoryUI);

export default NavbarSubCategory;
