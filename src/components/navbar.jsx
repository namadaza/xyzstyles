import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { StaticQuery, graphql } from 'gatsby';
import { connect } from 'react-redux';

import NavbarSubCategory from './navbar-subcategory';

import './styles/navbar.scss';

class NavbarUI extends Component {
  static propTypes = {
    menuLinks: PropTypes.arrayOf(PropTypes.shape({})),
    updateNavCategory: PropTypes.func.isRequired,
    selectedCategory: PropTypes.string.isRequired,
  };

  static defaultProps = {
    menuLinks: [{}],
  };

  updateCategory = event => {
    const { updateNavCategory } = this.props;
    const { currentTarget } = event;
    const newCategory = currentTarget.getElementsByClassName('product-category')[0].textContent;
    updateNavCategory(newCategory);
  };

  render() {
    const { menuLinks, selectedCategory } = this.props;

    return (
      <div className="xyz-navbar sticky-top">
        <div className="font-weight-bold pb-1">Product Showcase</div>
        {menuLinks.map(productCategory => {
          const { node } = productCategory;
          const { category, subCategories } = node;
          return (
            <div key={category}>
              <button type="button" className="navbar-button m-0 p-0 pb-1" onClick={this.updateCategory}>
                <div
                  className={classNames('product-category text-uppercase font-weight-normal', { 'font-weight-bold': selectedCategory === category })}
                >
                  {category}
                </div>
              </button>
              {selectedCategory === category && <NavbarSubCategory category={category} subCategories={subCategories} />}
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = ({ selectedCategory }) => ({ selectedCategory });

const mapDispatchToProps = dispatch => ({
  updateNavCategory: newCategory => dispatch({ type: `UPDATE_NAV_CATEGORY`, selectedCategory: newCategory }),
});

const ConnectedNavbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(NavbarUI);

const navbarQuery = graphql`
  {
    allProductsJson(limit: 1000) {
      edges {
        node {
          category
          subCategories {
            name
            displayName
            products {
              productType
            }
          }
        }
      }
    }
  }
`;

const Navbar = () => <StaticQuery query={navbarQuery} render={result => <ConnectedNavbar menuLinks={result.allProductsJson.edges} />} />;

export default Navbar;
