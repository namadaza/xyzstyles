/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const Promise = require(`bluebird`);
const path = require(`path`);
const slash = require(`slash`);
const lodash = require('lodash');

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  const ProductCategoryTemplate = path.resolve(`src/templates/product-category-template.jsx`);

  // page titles add 's to name of category plus product type
  // ie. men and jackets becomes Men's Jacket
  // subCategory omitted
  const createPageTitle = (category, productType) =>
    `${lodash.capitalize(category)}${category === 'kids' ? `'` : `'s`} ${lodash.capitalize(productType)}`;

  // image paths follow pattern images\\category\\subCategory\\productType
  // subCategory omitted if none exist
  // ie. images\\men\\jackets
  // strange backslash sytnax since need to escape twice for "\\"
  const createProductImagesPath = (category, subCategory, productType) => `images/${category}${subCategory ? `/${subCategory}` : ''}/${productType}`;
  const createProductImagesPathDev = (category, subCategory, productType) => `images\\${category}${subCategory ? `\\${subCategory}` : ''}\\${productType}`;

  const createSlugs = (category, productType) =>
    createPage({
      path: `/${category}/${productType}`,
      component: slash(ProductCategoryTemplate),
      context: {
        pageTitle: createPageTitle(category, productType),
        productImages: createProductImagesPathDev(category, '', productType),
        category,
        productType,
      },
    });

  const createSlugsWithSubcategory = (category, subCategory, productType) =>
    createPage({
      path: `/${category}/${subCategory}/${productType}`,
      component: slash(ProductCategoryTemplate),
      context: {
        pageTitle: createPageTitle(category, productType),
        productImages: createProductImagesPathDev(category, subCategory, productType),
        category,
        subCategory,
        productType,
      },
    });

  return new Promise((resolve, reject) => {
    resolve(
      graphql(
        `
          {
            allProductsJson(limit: 1000) {
              edges {
                node {
                  id
                  category
                  subCategories {
                    name
                    products {
                      productType
                    }
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          reject(new Error(result.errors));
        }

        // Create product pages
        result.data.allProductsJson.edges.forEach(edge => {
          const { node } = edge;
          if (node.subCategories[0].name === 'default') {
            // Default case, category has no subCategories ie. man / woman
            node.subCategories[0].products.forEach(product => createSlugs(node.category, product.productType));
          } else {
            // ie. kids has subcategory boys | 5 to 15 years
            node.subCategories.forEach(subCategory =>
              subCategory.products.forEach(product => createSlugsWithSubcategory(node.category, subCategory.name, product.productType))
            );
          }
        });
      })
    );
  });
};
